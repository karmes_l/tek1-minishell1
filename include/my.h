/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Sun Feb  1 16:38:46 2015 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include <features.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <sys/types.h>
# include <dirent.h>
# include <signal.h>
# include <stdlib.h>
# include <unistd.h>
# include "get_next_line.h"
# include "minishell.h"

# define MAX(v1, v2)	((v1) > (v2)) ? (v1) : (v2)

char		*convert_base(char *, char *, char *);
int		count_num(long nb);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putchar0(char);
void		my_putcharerror(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
void		my_putstr0(char *);
void		my_putstrerror(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*my_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
char		*my_strdup(char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
int		my_putchar2(int);
char		**my_str_to_wordtab(char *, char);
void		minishell1(char **);
void		print_path(char **);
void		read_command(t_env *);
void		epure_str(char *);
int		fct_cd(t_env *, char **, int);
int		fct_setenv(t_env *, char **, int);
int		fct_unsetenv(t_env *, char **, int);
int		fct_env(char **, char **, int);
int		fct_exit(char **);
int		my_strtablen(char **);
void		free_tab(char **);
void		parser_path(char ***, char *);
void		parser_home(char **, char *);
void		init_environ(t_env *, char **);
int		find_bin(char *, char *);
int		exec_bin(char *, char **, char **);
void		error_notfound(char *);
void		error_path(char *);
void		error_env_notfound(char *);
int		error_env_args(char *);
void		new_element(char **, char *, char *, char *);
char		**char2_tab_realloc(char **, int);
void		remove_element_char2_tab(char ***, int);
void		*pmalloc(int);
int		kill(pid_t, int);
void		swap_str_left(char *, int);
void		path_parent(char *);
void		update_environ(t_env *, char *, char *);

#endif /* !MY_H_ */
