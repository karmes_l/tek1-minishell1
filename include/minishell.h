/*
** events.h for  in /home/karmes_l/Projets/Igraph/Wolf3D/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 20 11:47:45 2014 lionel karmes
** Last update Sun Feb  1 16:27:30 2015 lionel karmes
*/

#ifndef MINISHELL_H_
# define MINISHELL_H_

typedef struct	s_env
{
  char		**path;
  char		*home;
  char		*pwd;
  char		*oldpwd;
  char		**env;
}		t_env;

extern sig_atomic_t	g_signal_int;

# define END	(2)

#endif /* !MINISHELL_H_ */
