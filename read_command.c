/*
** read_command.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 17:15:51 2015 lionel karmes
** Last update Sun Feb  1 16:16:41 2015 lionel karmes
*/

#include "my.h"

extern sig_atomic_t	g_signal_int;

int	find_command_next(t_env *environ, char **args)
{
  int	i;
  char	*file_name;

  i = 0;
  if (exec_bin(args[0], args, environ->env))
    return (1);
  if (environ->path == NULL)
    return (0);
  while (environ->path[i] != 0)
    {
      if (find_bin(environ->path[i], args[0]))
	{
	  new_element(&file_name, environ->path[i], "/", args[0]);
	  exec_bin(file_name, args, environ->env);
	  free(file_name);
	  return (1);
	}
      i++;
    }
  return (0);
}

int	find_command(t_env *environ, char **args, int ac)
{
  int	cmd_found;

  cmd_found = 0;
  cmd_found = (!my_strcmp("cd", args[0])) ?
    fct_cd(environ, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("setenv", args[0])) ?
    fct_setenv(environ, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("unsetenv", args[0])) ?
    fct_unsetenv(environ, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("env", args[0])) ?
    fct_env(environ->env, args, ac) : cmd_found;
  cmd_found = (!my_strcmp("exit", args[0])) ? 2 : cmd_found;
  if (!cmd_found)
    if (!find_command_next(environ, args))
      error_notfound(args[0]);
  if (cmd_found == END)
    return (END);
  return (1);
}

void	get_sigint(int sign)
{
  g_signal_int = sign;
  my_putchar('\n');
}

void	read_signal()
{
  g_signal_int = 0;
  if (signal(SIGINT, get_sigint) == SIG_ERR)
    my_putstrerror("[ERROR] : SIGINT");
}

void	read_command(t_env *environ)
{
  char	*buff;
  char	**args;
  int	ac;
  int	end;

  end = 0;
  while (!end)
    {
      my_putstr("?>");
      read_signal();
      buff = get_next_line(0);
      if (buff == NULL && g_signal_int != SIGINT)
	break;
      else if (g_signal_int == SIGINT)
	continue;
      epure_str(buff);
      args = my_str_to_wordtab(buff, ' ');
      ac = my_strtablen(args);
      if (ac > 0)
	if (find_command(environ, args, ac) == END)
	  end = 1;
      free(buff);
      free_tab(args);
    }
}
