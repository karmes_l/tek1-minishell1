##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Sun Feb  1 16:43:34 2015 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/ 

LDFLAGS	=

NAME	= mysh

LIB	= libmy

SRCS	= main.c \
	minishell1.c \
	init_environ.c \
	get_next_line.c \
	epure_str.c \
	read_command.c \
	fct_cd.c \
	fct_env.c \
	update_environ.c \
	my_strtablen.c \
	find_bin.c \
	message.c \
	char2_tab_realloc.c \
	swap_str.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	make -C lib/
	$(CC) $(OBJS) -o $(NAME) $(CFLAGS) $(LDFLAGS) -L./lib/ -lmy

clean:
	$(RM) $(OBJS)
	make clean -C lib/

fclean: clean
	make fclean -C lib/
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
