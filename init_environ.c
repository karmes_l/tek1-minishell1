/*
** parser_path.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Jan 11 17:42:10 2015 lionel karmes
** Last update Sun Feb  1 17:17:08 2015 lionel karmes
*/

#include "my.h"

void	print_path(char **path)
{
  int	i;

  i = 0;
  if (path != NULL)
    {
      while (path[i] != NULL)
	{
	  my_putstr(path[i]);
	  my_putchar('\n');
	  i++;
	}
    }
  else
    my_putstrerror("ERROR : PATH = NULL\n");
}

void	parser_path(char ***path, char *env)
{
  int	c;
  int	i;
  char	*tmp;

  c = 0;
  i = 0;
  if (env == NULL)
    *path = NULL;
  else
    {
      while (env[c] != '=' && env[c] != '\0')
	c++;
      c++;
      if ((tmp = pmalloc(sizeof(char) * (my_strlen(env) - c + 1))) == NULL)
	exit(1);
      while (c < my_strlen(env))
	{
	  tmp[i] = env[c];
	  i++;
	  c++;
	}
      tmp[i] = '\0';
      *path = my_str_to_wordtab(tmp, ':');
      free(tmp);
    }
}

void	parser_home(char **home, char *env)
{
  int	c;
  int	k;

  c = 0;
  k = 0;
  if (env == NULL)
    *home = NULL;
  else
    {
      while (env[c] != '=' && env[c] != '\0')
	c++;
      if ((*home = pmalloc(sizeof(char) * (my_strlen(env) - c))) == NULL)
	exit(1);
      c++;
      while (c + k < my_strlen(env))
	{
	  (*home)[k] = env[c + k];
	  k++;
	}
      (*home)[k] = '\0';
    }
}

void	init_env(char ***env_cpy, char **env_ref, int size)
{
  int	i;

  i = 0;
  *env_cpy = pmalloc(sizeof(char *) * (size + 1));
  (*env_cpy)[size] = NULL;
  while (i < size)
    {
      (*env_cpy)[i] = pmalloc(sizeof(char) * my_strlen(env_ref[i]) + 1);
      my_strcpy((*env_cpy)[i], env_ref[i]);
      i++;
    }
}

void	init_environ(t_env *environ, char **env)
{
  int	i;

  i = 0;
  environ->path = NULL;
  environ->home = NULL;
  environ->pwd = NULL;
  environ->oldpwd = NULL;
  environ->env = NULL;
  while (env[i] != NULL)
    {
      if (my_strncmp("PATH", env[i], 4) == 0)
	  parser_path(&(environ->path), env[i]);
      if (my_strncmp("HOME", env[i], 4) == 0)
	parser_home(&(environ->home), env[i]);
      if (my_strncmp("PWD", env[i], 4) == 0)
	parser_home(&(environ->pwd), env[i]);
      if (my_strncmp("OLDPWD", env[i], 4) == 0)
	parser_home(&(environ->oldpwd), env[i]);
      i++;
    }
  init_env(&(environ->env), env, i);
}
