/*
** fct_env.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 18:29:20 2015 lionel karmes
** Last update Sun Feb  1 17:25:56 2015 lionel karmes
*/

#include "my.h"

void	update_environ_path(t_env *environ, char *arg1, char *str)
{
  if (!my_strcmp(arg1, "PATH"))
    {
      if (environ->path != NULL)
	free_tab(environ->path);
      parser_path(&(environ->path), str);
    }
}

void	update_environ_home(t_env *environ, char *arg1, char *str)
{
  if (!my_strcmp(arg1, "HOME"))
    {
      if (environ->home != NULL)
	free(environ->home);
      parser_home(&(environ->home), str);
    }
}

void	update_environ_pwd(t_env *environ, char *arg1, char *str)
{
  if (!my_strcmp(arg1, "PWD"))
    {
      if (environ->pwd != NULL)
	free(environ->pwd);
      parser_home(&(environ->pwd), str);
    }
}

void	update_environ_oldpwd(t_env *environ, char *arg1, char *str)
{
  if (!my_strcmp(arg1, "OLDPWD"))
    {
      if (environ->oldpwd != NULL)
      	free(environ->oldpwd);
      parser_home(&(environ->oldpwd), str);
    }
}

void	update_environ(t_env *environ, char *arg1, char *arg2)
{
  char	*str;
  void	(*update_env[4])(t_env *, char *, char *);
  int	i;

  i = 0;
  if (arg2 != NULL)
    new_element(&str, arg1, "=", arg2);
  else
    str = NULL;
  update_env[0] = &update_environ_path;
  update_env[1] = &update_environ_home;
  update_env[2] = &update_environ_pwd;
  update_env[3] = &update_environ_oldpwd;
  while (i < 4)
    {
      update_env[i](environ, arg1, str);
	i++;
    }
  if (str != NULL)
    free(str);
}
