/*
** epure_str.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Jan 12 17:33:31 2015 lionel karmes
** Last update Thu Jan 22 16:34:15 2015 lionel karmes
*/

#include "my.h"
#include <stdlib.h>

void	decal_str(char *str, int i)
{
  while (str[i] != '\0')
    {
      str[i] = str[i + 1];
      i++;
    }
}

void	epure_str(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (i == 0 && str[i] == ' ')
	decal_str(str, i);
      else if (str[i] == ' ' && (str[i + 1] == ' ' || str[i + 1] == '\0'))
      	decal_str(str, i);
      else
	i++;
    }
}
