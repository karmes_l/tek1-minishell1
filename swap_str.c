/*
** swap_str.c for  in /home/karmes_l/Projets/Systeme_Unix/minishell1/v3/minishell1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Jan 31 15:22:26 2015 lionel karmes
** Last update Sun Feb  1 01:10:47 2015 lionel karmes
*/

#include "my.h"

void	swap_str_left(char *str, int nb)
{
  int	i;

  while (nb > 0)
    {
      i = 0;
      while (str[i] != '\0')
	{
	  str[i] = str[i + 1];
	  i++;
	}
      nb--;
    }
}

void	path_parent(char *str)
{
  int	i;

  i = my_strlen(str) - 1;
  while (i >= 0  && str[i] != '/')
    i--;
  if (i > 0)
    str[i] = '\0';
  else
    str[i + 1] = '\0';
}
